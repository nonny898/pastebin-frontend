import Vue from 'vue';
import Router from 'vue-router';
import Home from '../views/Home';
import Paste from '../views/Paste.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/:id',
      name: 'paste',
      component: Paste
    }
  ]
});
