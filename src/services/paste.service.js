/* eslint-disable no-console */
import axios from 'axios';

export default {
  async getById(id) {
    const res = await axios.get('api/' + id);
    return res.data;
  },
  async postRecent() {
    const res = await axios.post('api/recents', {});
    return res.data;
  },
  async postContent(title, content) {
    const res = axios.post('api/paste', {
      title: title,
      content: content
    });
    return res;
  }
};
